from __future__ import unicode_literals
from ornaments.transform.strings import printable, ascii, ascii_lower, ascii_upper, digits


def test_printable():
    unicode_string = 'Some string with an amount \u00A310.00 in pounds'

    @printable()
    def foo():
        return unicode_string

    def bar():
        return unicode_string

    assert foo() == 'Some string with an amount 10.00 in pounds'
    assert bar() == 'Some string with an amount \u00A310.00 in pounds'


def test_ascii():
    unicode_string = 'Some string with an amount \u00A310.00 in pounds'

    @ascii()
    def foo():
        return unicode_string

    def bar():
        return unicode_string

    assert foo() == 'Somestringwithanamountinpounds'
    assert bar() == 'Some string with an amount \u00A310.00 in pounds'


def test_ascii_lower():
    unicode_string = 'Some string with an amount \u00A310.00 in pounds'

    @ascii_lower()
    def foo():
        return unicode_string

    def bar():
        return unicode_string

    assert foo() == 'omestringwithanamountinpounds'
    assert bar() == 'Some string with an amount \u00A310.00 in pounds'


def test_ascii_upper():
    unicode_string = 'Some string with an amount \u00A310.00 in pounds'

    @ascii_upper()
    def foo():
        return unicode_string

    def bar():
        return unicode_string

    assert foo() == 'S'
    assert bar() == 'Some string with an amount \u00A310.00 in pounds'


def test_digits():
    unicode_string = 'Some string with an amount \u00A310.00 in pounds'

    @digits()
    def foo():
        return unicode_string

    def bar():
        return unicode_string

    assert foo() == '1000'
    assert bar() == 'Some string with an amount \u00A310.00 in pounds'
