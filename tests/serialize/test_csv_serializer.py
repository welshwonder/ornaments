from __future__ import unicode_literals
from ornaments.serialize import csv


def test_csv():

    @csv()
    def foo():
        return [{"field1": "content_1a", "field2": "content_2a", "field3": "content_3a"},
                {"field1": "content_1b", "field2": "content_2b", "field3": "content_3b"},
                {"field1": "content_1c", "field2": "content_2c", "field3": "content_3c"},
                ]
    assert foo() == '''field1,field2,field3\r
content_1a,content_2a,content_3a\r
content_1b,content_2b,content_3b\r
content_1c,content_2c,content_3c\r
'''


def test_csv_single_row():

    @csv()
    def foo():
        return [{"field1": "content_1a", "field2": "content_2a", "field3": "content_3a"},
                ]
    assert foo() == '''field1,field2,field3\r
content_1a,content_2a,content_3a\r
'''


def test_csv_empty_row():

    @csv()
    def foo():
        return [{},
                ]
    assert foo() == '\r\n'


def test_csv_no_rows():

    @csv()
    def foo():
        return []
    assert foo() == ''


def test_csv_with_field_names():

    @csv(field_names=['field1', 'field3'])
    def foo():
        return [{"field1": "content_1a", "field2": "content_2a", "field3": "content_3a"},
                {"field1": "content_1b", "field2": "content_2b", "field3": "content_3b"},
                {"field1": "content_1c", "field2": "content_2c", "field3": "content_3c"},
                ]
    assert foo() == '''field1,field3\r
content_1a,content_3a\r
content_1b,content_3b\r
content_1c,content_3c\r
'''


def test_csv_no_rows_with_field_names():

    @csv(field_names=['field1', 'field3'])
    def foo():
        return []
    assert foo() == '''field1,field3\r
'''


def test_csv_without_header():

    @csv(header=False)
    def foo():
        return [{"field1": "content_1a", "field2": "content_2a", "field3": "content_3a"},
                {"field1": "content_1b", "field2": "content_2b", "field3": "content_3b"},
                {"field1": "content_1c", "field2": "content_2c", "field3": "content_3c"},
                ]
    assert foo() == '''content_1a,content_2a,content_3a\r
content_1b,content_2b,content_3b\r
content_1c,content_2c,content_3c\r
'''