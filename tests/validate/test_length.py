import pytest
from ornaments.validate import length


def test_length_exact():

    @length(exact=5)
    def matches():
        return 'exact'

    @length(exact=11)
    def greater_than():
        return 'greater_than'

    @length(exact=10)
    def less_than():
        return 'less_than'

    assert matches() == 'exact'
    with pytest.raises(AssertionError) as exec_info:
        greater_than()
    assert 'Invalid length 12 != 11' == str(exec_info.value)
    with pytest.raises(AssertionError) as exec_info:
        less_than()
    assert 'Invalid length 9 != 10' == str(exec_info.value)


def test_length_minimum():

    @length(minimum=5)
    def exact():
        return 'exact'

    @length(minimum=11)
    def greater_than():
        return 'greater_than'

    @length(minimum=10)
    def less_than():
        return 'less_than'

    assert exact() == 'exact'
    assert greater_than() == 'greater_than'
    with pytest.raises(AssertionError) as exec_info:
        less_than()
    assert 'Invalid length 9 < 10' == str(exec_info.value)


def test_length_maximum():

    @length(maximum=5)
    def exact():
        return 'exact'

    @length(maximum=11)
    def greater_than():
        return 'greater_than'

    @length(maximum=10)
    def less_than():
        return 'less_than'

    assert exact() == 'exact'
    with pytest.raises(AssertionError) as exec_info:
        greater_than()
    assert 'Invalid length 12 > 11' == str(exec_info.value)
    assert less_than() == 'less_than'


def test_length_minimum_and_maximum():

    @length(minimum=5, maximum=5)
    def exact():
        return 'exact'

    @length(minimum=6, maximum=8)
    def between():
        return 'between'

    @length(minimum=5, maximum=11)
    def greater_than():
        return 'greater_than'

    @length(minimum=10, maximum=12)
    def less_than():
        return 'less_than'

    assert exact() == 'exact'
    assert between() == 'between'
    with pytest.raises(AssertionError) as exec_info:
        greater_than()
    assert 'Invalid length 12 > 11' == str(exec_info.value)
    with pytest.raises(AssertionError) as exec_info:
        less_than()
    assert 'Invalid length 9 < 10' == str(exec_info.value)


def test_length_invalid_kwargs_must_define_one():
    with pytest.raises(AssertionError) as exec_info:
        @length()
        def invalid():
            return 'invalid'
        invalid()
    assert 'Must define one or more of exact, minimum, maximum kwargs' == str(exec_info.value)


def test_length_invalid_kwargs_cant_define_exact_minimum_maximum():
    with pytest.raises(AssertionError) as exec_info:
        @length(exact=5, minimum=3, maximum=7)
        def invalid():
            return 'invalid'
        invalid()
    assert 'Cannot define both exact and minimum/maximum' == str(exec_info.value)


def test_length_invalid_kwargs_cant_define_exact_minimum():
    with pytest.raises(AssertionError) as exec_info:
        @length(exact=5, minimum=3)
        def invalid():
            return 'invalid'
        invalid()
    assert 'Cannot define both exact and minimum/maximum' == str(exec_info.value)


def test_length_invalid_kwargs_cant_define_exact_maximum():
    with pytest.raises(AssertionError) as exec_info:
        @length(exact=5, maximum=7)
        def invalid():
            return 'invalid'
        invalid()
    assert 'Cannot define both exact and minimum/maximum' == str(exec_info.value)
