install:
	poetry install

run-pytest:
	poetry run pytest tests

publish:
	poetry publish --build --username="__token__" --password="${PYPI_TOKEN}"
